var express = require('express');
var router = express.Router();
// const device = require('./device');
const moment = require('moment');

// router.get('/', device.showStatus);
// router.get('/device/sinyal', device.getStatus);
// router.post('/device/sinyal', device.addDevice);
// router.put('/device/sinyal', createRequestLog, device.updateStatus);

router.get('/', (req, res) => {
  res.send('Server work perfectly!!');
});
router.all('/webhook', (req, res) => {
  let resp = null;
  if (req.body.phone === '6282219230359') {
    resp = req.body;
  }
  if (req.body.phone === '6281223122798') {
    resp = req.body;
  }
  if (String(req.body.fromMe).toString() === 'true') {
    resp = req.body;
  }
  res.send(resp);
});


// async function createRequestLog(req, res, next) {
//   const requestId = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
//   const dikirim = moment(req.query.requestTime);
//   const diterima = moment(new Date().getTime());
//   req.requestId = requestId;
//   global.rekapWaktu[requestId] = {
//       id: requestId,
//       data_dikirim: dikirim,
//       data_diterima: diterima,
//       data_update_selesai: 0,
//       fuzzy_selesai: 0,
//       proses_selesai: 0
//   }
//   next();
// }

module.exports = router;
